" Name the program.
let g:program_to_debug="main.elf"

" Set the debugger.
let termdebugger='arm-none-eabi-gdb'

" Set of commands to run after launching GDB.
let post_debug_commands = ['target remote :3333', 'mon halt', 'load', 'mon reset halt']

" Run some command in the background.
term ++hidden openocd -f stm32f4discovery.cfg

" Edit the main file.
e main.cpp
