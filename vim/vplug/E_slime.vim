" Slime: execute in vim terminal!
Plug 'jpalardy/vim-slime'

function s:configure()
    " Specify the "terminal" to use
    let g:slime_target = "vimterminal"
endfunc

call vplug#add_conf({-> s:configure() })
