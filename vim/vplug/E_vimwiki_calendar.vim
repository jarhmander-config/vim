" Plugin and configuration for vimwiki + its calendar

" VimWiki and a calendar
" NOTE: vimwiki is heavy, avoid loading it unless necessary
Plug 'vimwiki/vimwiki', {
\   'for' : 'markdown',
\   'on'  : ['VimwikiMakeDiaryNote', 'VimwikiIndex']
\}
Plug 'mattn/calendar-vim'

let g:vimwiki_ext2syntax = {'.md': 'markdown',
                  \ '.mkd': 'markdown',
                  \ '.wiki': 'media'}
