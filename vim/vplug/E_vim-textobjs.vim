" Easier text objects
Plug 'kana/vim-textobj-user'

" Custom text objects:
" Entire buffer
Plug 'kana/vim-textobj-entire'

" Function parameters
Plug 'sgur/vim-textobj-parameter'
let g:vim_textobj_parameter_mapping = 'a'
