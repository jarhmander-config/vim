" Colorscheme
Plug 'morhetz/gruvbox'

function s:configure()
    colorscheme gruvbox
    let g:gruvbox_contrast_dark='hard'
    set background=dark
    " Colors hacks...
    highlight Search cterm=bold ctermbg=242
endfunc

call vplug#add_conf({-> s:configure() })
