" Lisp goodies
" Must look at this: https://blog.jeaye.com/2016/11/30/vim-clojure/
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'tpope/vim-repeat'
