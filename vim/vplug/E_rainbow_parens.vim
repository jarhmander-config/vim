" Multi-color parens
Plug 'junegunn/rainbow_parentheses.vim'

" For rainbow_parentheses.vim:
" Activation based on file type
augroup rainbow_lisp
  autocmd!
  autocmd FileType lisp,clojure,scheme,racket,c,cpp RainbowParentheses
augroup END
let g:rainbow#pairs = [['(', ')'], ['[', ']']]
