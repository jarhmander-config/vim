" "Universal" completer
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Plugins
Plug 'fannheyward/coc-marketplace', {'do': 'yarn install --frozen-lockfile'}
