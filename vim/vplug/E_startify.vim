" Default page
Plug 'mhinz/vim-startify'

function s:configure()
    let g:startify_fortune_use_unicode = 1

    let ascii_art = [
     \ '   __     _                     ',
     \ '  /\ \  /| |                    ',
     \ '  \ \ \ || |                    ',
     \ '   \ \ \|| | __   __            ',
     \ '    \ \ \| |/\_\ /\ `¯¯`v¯¯`\   ',
     \ '     \ \   |\/\¯\\ \ \¯\ \¯\ \  ',
     \ '      \ \__| \ \_\\ \_\ \_\ \_\ ',
     \ '       \/_/   \/_/ \/_/\/_/\/_/ ',
     \ '']

    let g:startify_custom_header = startify#pad(ascii_art + startify#fortune#boxed())
endfunc

call vplug#add_conf({-> s:configure()})
