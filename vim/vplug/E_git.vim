" git commands
Plug 'tpope/vim-fugitive'

" git history viewer
Plug 'junegunn/gv.vim'

" Automatically fold anything git related
autocmd FileType git* setlocal foldmethod=syntax
