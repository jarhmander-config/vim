let s:configure_funcs = []

function! vplug#add_conf(fn)
    let s:configure_funcs += [ a:fn ]
endfunc

function! vplug#load()
    " vim-plug magic
    call plug#begin('~/.local/share/nvim/plugged')
    run! vplug/E_*.vim
    call plug#end()
endfunc

function! vplug#configure_after()
    for i in range(len(s:configure_funcs))
        call call(s:configure_funcs[i], [])
    endfor
endfunc
