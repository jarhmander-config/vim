" my filetype file
if exists("did_load_filetypes")
    finish
endif
augroup filetypedetect
    au! BufRead,BufNewFile *.x       setfiletype c
    au! BufRead,BufNewFile *.sls     setfiletype scheme
augroup END
