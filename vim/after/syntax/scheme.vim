" Standard scheme
syn keyword schemeSyntaxSyntax with-ellipsis
syn keyword schemeSyntaxSyntax with-syntax
syn keyword schemeSyntaxSyntax syntax-case
syn keyword schemeSyntaxSyntax with-implicit
syn keyword schemeLibrarySyntax library import export only rename except
syn keyword schemeSyntax lambda* case-lambda*
syn keyword schemeSyntaxSyntax meta

syn keyword schemeSyntaxSyntax fields protocol mutable immutable parent sealed
syn keyword schemeSyntaxSyntax opaque nongenerative generative parent-rtd

" Chez Scheme
syn keyword schemeSyntax define-record


" Guile
syn keyword schemeLibrarySyntax define-module
syn keyword schemeSyntaxSyntax define-syntax-rules

"syn keyword schemeFunction *
