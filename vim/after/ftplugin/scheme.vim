" Syntax
setl lispwords+=with-ellipsis,with-syntax
setl lispwords+=syntax-case
setl lispwords+=define-syntax-rules
setl lispwords+=with-implicit,with-slots
setl lispwords+=lambda*,case-lambda*
setl lispwords+=meta

" I/O
setl lispwords+=with-input-from-file,with-input-from-port,with-input-from-string
setl lispwords+=with-output-to-file,with-output-to-port,with-output-to-string
setl lispwords+=with-error-to-file,with-error-to-port,with-error-to-string
setl lispwords+=call-with-input-file,call-with-input-string
setl lispwords+=call-with-output-file,call-with-output-string

" Control structures
setl lispwords+=call-with-prompt
setl lispwords+=call/cc,call-with-current-continuation
setl lispwords+=call/ec,let/ec,call/1cc

" Fluids (Guile)
setl lispwords+=with-fluid*,with-fluids,with-fluids*

" Guile functions
setl lispwords+=with-throw-handler,with-readline-completion-function,with-mutex
setl lispwords+=with-continuation-barrier,with-dynamic-state
setl lispwords+=call-with-blocked-asyncs,call-with-deferred-observers,call-with-dynamic-root
setl lispwords+=call-with-module-autoload-lock,call-with-new-thread
setl lispwords+=call-with-unblocked-asyncs

setl sw=2
setl ts=8
