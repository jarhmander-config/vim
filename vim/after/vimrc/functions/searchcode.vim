" Helper to search for similar code.
"

function! s:search_similar_code(line1, line2, var='')
    if !empty(a:var)
        let code = eval(a:var)
    else
        let code = join(getline(a:line1, a:line2))
    endif
    let code = escape(trim(code), '\[]*')
    let code = substitute(code, '\(\(\<\|\>\)\_s*\)\|\(\_s\+\)', '\\_s*', 'g')
    let code = substitute(code, '\(\\_s\*\)\+', '\1', 'g')
    let code = substitute(code, '^\\_s\*', '', '')
    let code = substitute(code, '\\_s\*$', '\1', '')
    call feedkeys('/\m' . code . "\<CR>", "n")
endfunc

command! -range -nargs=?  SearchCode call s:search_similar_code(<line1>, <line2>, <f-args>)
