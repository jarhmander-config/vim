" Quick hack: close buffer, and open it in GVim.
function! s:open_in_gvim(rw) abort
    let fn = @%
    let options = "-R "
    if a:rw
        bw
        let options = ""
    endif
    silent exec(printf('!gvim %s%s >/dev/null 2>&1 &', options, shellescape(l:fn)))
    redraw!
endfunc

command! -bang GVim call s:open_in_gvim("!" == "<bang>")
