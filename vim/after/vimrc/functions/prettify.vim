"
" Simple prettifier for code.
"

command! Prettify exec 'retab | set ff=unix | %s/ \+$//ce'
