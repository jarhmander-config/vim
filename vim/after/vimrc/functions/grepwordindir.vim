
function! GrepWordInDir(word)
    exec "grep --include='*.[ch]' --include='*.[ch]pp' -r -e " . a:word . " . "
endfunction

"grep the word under cursor
"
command! -nargs=1 GrepInDir call GrepWordInDir(<f-args>)


