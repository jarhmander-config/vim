" Compare snips

let s:cnt = 0

function! s:create_new_scratch()
    let winnr = bufwinnr("diff-" . s:cnt)
    if winnr > 0
        exec winnr 'wincmd w'
        rightbelow vnew
    else
        new
    endif
    let s:cnt+=1
    exec "file diff-" .  s:cnt
    setlocal buftype=nofile bufhidden=hide noswapfile
    diffthis
    setlocal diffopt+=iwhite
endfunc

function s:compare_snip(line1, line2)
    let lines = getline(a:line1, a:line2)
    call s:create_new_scratch()
    call append(0, lines)
    redraw!
    wincmd w
endfunc

command! -range CompareSnip call s:compare_snip(<line1>, <line2>)
