"
" mkheader: Make file headers in C and C++ header and source files
"

" Receive a list of lines, returns a list of lines that wraps the lines.
func! s:wrap_in_comments_doc(list)
    let out = ['/**']
    for e in a:list
        call add(out, s:rtrim(' * ' . e))
    endfor
    return add(out, ' */')
endfunc

" Trim whitespace from both sides
func! s:trim(str)
    return substitute(a:str, '^[ \t\n]*\(\S.\{-}\)[ \t\n]*$', '\1', '')
endfunc

" Trim trailing whitespace
func! s:rtrim(str)
    return substitute(a:str, '[ \t\n]*$', '', '')
endfunc

" Get name from git, otherwise 'anonymous'
func! s:get_name()
    let name = s:trim(system('git config user.name'))
    if v:shell_error != 0
        let name = 'anonymous'
    endif
    return name
endfunc

" Get email from git, otherwise some default
func! s:get_email()
    let email = s:trim(system('git config user.email'))
    if v:shell_error != 0
        let email = 'anonymous@anon.local'
    endif
    return email
endfunc

" Format nicely the name and email
func! s:format_name_and_email()
    let name = s:get_name()
    let email = s:get_email()
    return printf("%s <%s>", name, email)
endfunc

" Generate fine header (in a list)
func! s:make_file_header()
    if exists("g:mkheader_file_header")
        let ret = g:mkheader_file_header
        if type(ret) == v:t_string
            let ret = split(ret, "\n")
        end
        " Assume it's a list...
        return ret
    else
        return s:wrap_in_comments_doc([
                    \ '@file',
                    \ '@author ' . s:format_name_and_email(),
                    \ '',
                    \ '@brief'
                    \ ])
    endif
endfunc

" Receive a filename, output a suitable macro name for include guards
" (ex: src/foo.h -> FOO_H)
func! s:filename_to_macro(filename)
    let strip = fnamemodify(a:filename, ':t')
    return substitute(toupper(strip), "\\.", "_", "g")
endfunc

" From a filename, generate a include guard (in a list).
" The include guard has 2 empty lines in it; we should insert stuff from line
" 3.
func! s:make_include_guards(filename)
    let macro = s:filename_to_macro(a:filename)
    return [
                \ '#ifndef ' . macro,
                \ '#define ' . macro,
                \ '', '',
                \ '#endif // ' . macro
                \ ]
endfunc

" Make an extern "C" declaration (in a list)
func! s:make_extern_c()
    return      [ "#ifdef __cplusplus", 'extern "C" {', "#endif",
                \ '', '',
                \ "#ifdef __cplusplus", '} // extern "C"', "#endif"
                \ ]
endfunc

" Simply prepare a "h" file header
func! s:do_h_header(filename, ext)
    call append(0, s:make_file_header())
    let l = line('$')
    call append(l, s:make_include_guards(a:filename))
    let l += 3
    if a:ext == "h"
        call append(l, s:make_extern_c())
        call cursor(l+4, 1)
    else
        call cursor(l, 1)
    endif
endfunc

" Prepare a "c/cpp" file
func! s:do_src_header(filename, ext)
    call append(0, s:make_file_header())
    let l = line('$')
    let stem = fnamemodify(a:filename, ':t:r')
    let include = printf('#include "%s.%s"', stem, (a:ext == 'c'? 'h': 'hpp'))
    call append(l, [include, ''])
    call cursor(line('$'), 1)
endfunc

" entry point for MkHeader command
func! s:do_mkheader()
    let filename = getreg('%')
    let ext = fnamemodify(filename, ':e')

    if strcharpart(ext, 0, 1) == "h"
        call s:do_h_header(filename, ext)
    else
        call s:do_src_header(filename, ext)
    endif
endfunc

" Entry point for MkFuncHeader command
func! s:do_mkdoc()
    let l = line('.')
    let strline = s:trim(getline(l))
    let doc = ['@brief']

    if empty(strline)
        delete _
    else
        " TODO: make something funny with function arguments
    endif
    let comment = s:wrap_in_comments_doc(l:doc)
    call append(l-1, comment)
    exec printf(":%d|normal =%%", l)
    call cursor(l+1, 1)
    normal A 
endfunc

"---
command! MkHeader call s:do_mkheader()
command! MkDoc call s:do_mkdoc()
