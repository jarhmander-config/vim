"
" Functions dealing with time
"

" @brief Convert a time duration to a string representing time.
"
" @param  min Number of minutes.
" @return String representing time (ex: '3min', '1h35')
function! s:minutes_to_formatted_time(min)
    let hours = float2nr(a:min / 60)
    let minutes = a:min - 60 * hours
    return abs(hours) > 0? printf("%dh%02.0g", hours, minutes) : printf("%dmin", minutes)
endfunc

" @brief Decode time/duration to a number of minutes
"
" This converts things like '1h35' to 95 (a number).
"
" @param  sftime Time/duration to decode (string)
" @return Number of minutes (number)
function! s:decode_formatted_time_to_minutes(sftime)
    let ctx = {}
    function! ctx.sub(m)
        let h = str2nr(a:m[1])
        let m = str2nr(a:m[3])
        let r = 0

        if a:m[2] == ":" || a:m[2] == "h"
            let r = 60*h + m
        elseif a:m[2] == "m"
            let r = h
        end
        return r
    endfunc

    return str2nr(substitute(a:sftime, '^\(\d\+\)\([:hms]\)\(\d*\)', function(ctx.sub), ""))
endfunc

" @brief Add its time/duration arguments
"
" This adds all arguments, which can be either numbers (which are interpreted
" as minutes) or strings (which are interpreted according to
" `s:decode_formatted_time_to_minutes`) and give that result.
"
" @param  ... Time/duration arguments.
" @return Sum of all the arguments.
function! s:add_formatted_time(first, ...)
    let sum = s:decode_formatted_time_to_minutes(a:first)
    for e in a:000
        let sum += s:decode_formatted_time_to_minutes(e)
    endfor
    return sum
endfunc

" @brief Substract its time/duration arguments
"
" This is like `s:add_formatted_time`, except it substracts instead of adds
" its argument.
"
" @param  ... Time/duration arguments.
" @return Difference of all the arguments.
function! s:sub_formatted_time(first, ...)
    let sum = s:decode_formatted_time_to_minutes(a:first)
    for e in a:000
        let sum -= s:decode_formatted_time_to_minutes(e)
    endfor
    return sum
endfunc

" @brief Scan lines and add durations found in them
"
" This utility scans lines given in arguments and adds the duration found in
" these. Each argument can be a number, which designate a specific line, of a
" 2-element list, which designates a range [first last] (all inclusive) of
" lines to scan. It is assumed that durations are at the end of lines, in
" parens, with optional trailing space
"
" @param  ... List of lines to scan according to the description above
" @return Sum of all durations found
function! s:cummulate_time_found_in_lines(...)
    let sum = 0
    for e in a:000
        if type(e) == v:t_list
            let [line1, line2] = e
            for i in range(line1, line2)
                let l = getline(i)
                let m = matchlist(l, '(\(\d\+[hms:][^)]*\)) *$')
                if !empty(m)
                    let sum+=s:decode_formatted_time_to_minutes(m[1])
                endif
            endfor
        else
            let sum+=s:cummulate_time_found_in_lines([e, e])
        endif
    endfor
    return sum
endfunc

" @copydoc s:minutes_to_formatted_time
function! Min2ftime(min)
    return s:minutes_to_formatted_time(a:min)
endfunc

" @copydoc s:decode_formatted_time_to_minutes
function! Decodeftime(s)
    return s:decode_formatted_time_to_minutes(a:s)
endfunc

" @brief Add time/duration with conversion
"
" Like `s:add_formatted_time`, but convert the output with
" `s:minutes_to_formatted_time`.
function! AddTime(...)
    return s:minutes_to_formatted_time(call('s:add_formatted_time', a:000))
endfunc

" @brief Subtract time/duration with conversion
"
" Like `s:sub_formatted_time`, but convert the output with
" `s:minutes_to_formatted_time`.
function! SubTime(...)
    return s:minutes_to_formatted_time(call('s:sub_formatted_time', a:000))
endfunc

" @brief Scan lines and add durations found in them
"
" Like `s:cummulate_time_found_in_lines`, but convert the output with
" `s:minutes_to_formatted_time` and put the result in the `t` register.
function! CummulateTimeLines(...)
    let ret = s:minutes_to_formatted_time(call('s:cummulate_time_found_in_lines', a:000))
    let @t = ret
    return ret
endfunc
