let s:si_suffix = { -4 : 'p',
                  \ -3 : 'n',
                  \ -2 : 'µ',
                  \ -1 : 'm',
                  \  0 : '',
                  \  1 : 'k',
                  \  2 : 'M',
                  \  3 : 'G',
                  \  4 : 'T' }

function! Engfmt(value)
    let pow_1000 = (a:value == 0? 0 : float2nr(floor(log(abs(a:value)) / log(1000))))
    return printf("%.3g%s", a:value / pow(1000, pow_1000), s:si_suffix[pow_1000])
endfunc
