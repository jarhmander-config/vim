nnoremap <F5>  :wa <Bar> make<CR>
nnoremap <F10> :wqa<CR>
nnoremap <F9>  :confirm quit<CR>
nnoremap <Space> <Space>:noh <Bar> echo<CR>

nnoremap <F2> :cprev<CR>
nnoremap <F3> :cnext<CR>

nnoremap <Silent> <F5> :make<CR>
nnoremap <F7> [[zz
nnoremap <F8> ]]zz
nnoremap <silent> <F9> :mkses!<CR>
nnoremap <silent> <F10> :confirm qa<CR>
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

" Need function GrepWordInDir
nnoremap <F4> :call GrepWordInDir('''\<' . expand('<cword>') . '\>''')<CR>

" Need NERDTree
nnoremap <Esc><C-O> :NERDTreeFocus<CR>

nnoremap <silent> <C-N> :if strlen(&mouse)<CR>let &mouse=''<CR>let &number=0<CR>else<CR>let &mouse='a'<CR>let &number=1<CR>endif<CR>
