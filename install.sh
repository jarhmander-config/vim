#!/bin/bash
mkdir -p ~/.env
ln -sirT vim ~/.vim
ln -sirT vim ~/.config/nvim
ln -sirT vimrc ~/.vimrc
ln -sir environ ~/.env/vim.env
mkdir -p ~/.vim/autoload
ln -sir ~/.local/share/nvim/site/autoload/plug.vim ~/.vim/autoload/plug.vim
echo "NOTE:"
echo "NOTE: nvim may need other packages like 'python-neovim' and 'python2-neovim'."
echo "NOTE: it's a good idea to :CheckHealth under nvim."
echo "NOTE: For YouCompleteMe you'll also need 'cmake' and 'libtinfo' ('ncurses5-compat-libs' in the AUR)"
echo "NOTE:"
