#!/usr/bin/env -S vim -S

nnoremap <F6>       :wa<Bar>StartGdb<CR>
nnoremap [15;2~   :wa<Bar>make test<CR>
imap <F6>           <C-O><F6>
imap [15;2~       <C-O>[15;2~
nnoremap <Leader>md :MkDoc<CR>
nnoremap <Leader>fi :YcmCompleter FixIt<CR>
nnoremap <C-BSlash><C-BSlash> :YcmCompleter GoTo<CR>
"Mouse support!
map <C-LeftMouse> <LeftMouse>:YcmCompleter GoTo<CR>

let termdebugger = "gdb"

if file_readable("boot-local.vim")
    source boot-local.vim
endif

function! s:mk_gdb_layout()
    Gdb
    exe "normal \<C-W>H"
    Source
    exe "normal \<C-W>K"
    exe "normal \<C-W>20+"
endfunc

function s:run_debug_commands(varname)
    for c in get(g:, a:varname, [])
        call TermDebugSendCommand(c)
    endfor
endfunc

function s:start_gdb() abort
    tabe %
    exec "Termdebug " . g:program_to_debug
    call s:run_debug_commands('post_debug_commands')
endfunc

command! StartGdb call s:switch_to_gdb_or_launch()

function s:find_tab_by_buffer_name(name)
    for i in range(tabpagenr('$'))
        let tabno = i + 1
        let buflist = tabpagebuflist(tabno)

        for j in buflist
            if a:name == bufname(j)
                return tabno
            endif
        endfor
    endfor
    return 0
endfunc

function s:switch_to_gdb_or_launch()
    if !exists("g:program_to_debug")
        echoerr printf("You must define '%s' to run this command!", "g:program_to_debug")
        return
    end
    let tabno = s:find_tab_by_buffer_name("!" . g:termdebugger)
    if tabno > 0
        exe printf("norm %dgt", tabno)
    else
        call s:start_gdb()
    endif
    call s:mk_gdb_layout()
endfun

VimEnableCpp
