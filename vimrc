"Not really necessary...
set nocompatible

syn on
set ai bs=indent,eol,start
set nu
set nowrap
set tabstop=4 shiftwidth=4
set expandtab smarttab
set incsearch hlsearch
set mouse=a
set hidden
set laststatus=2
set showcmd
set wildmode=longest,list,full
set wildmenu
set cursorline
set scrolloff=3
set autoread

" I do this all the time, let's include it.
set path=**

" Do not indent namespaces
set cinoptions=N-s

" For YouCompleteMe
set encoding=utf-8

set formatoptions+=tcrnlj
filetype on
filetype plugin on
filetype indent on

" Do not wait too long for ESC sequence. For multi key mapping however,
" wait up to 1sec
set timeoutlen=1000 ttimeoutlen=10

" Load all startup addons (the ones I write)
run! vimrc/**/*.vim vimrc/*.vim

" from the vim-plug FAQ, adapted a bit for nvim usage.
" BUGGY: mustfix. Completely unreliable because of bad paths used here (my
" fault!). At least, once installed, it does work correctly.
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call vplug#load()
call vplug#configure_after()

function s:enable_cpp()
    redraw | echomsg "Loading plugins. please be patient..."
    "call plug#load('vim-vebugger')
    packadd termdebug
    redraw | echomsg "Done loading plugins."
endfunc

" Include some setup file for Coc, that I'll not just directly include here...
exec printf('source %s/%s', fnamemodify(resolve(expand('$MYVIMRC')), ':p:h'), 'coc-setup.vim')


command VimEnableCpp call s:enable_cpp()
