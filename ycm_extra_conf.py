import shlex
import os
import subprocess
from time import ctime
import re

LOG_NONE   = 0
LOG_ERROR  = 1
LOG_WARN   = 2
LOG_MSG    = 3
LOG_INFO   = 4
LOG_DEBUG  = 5

LOGLEVEL = LOG_MSG

log_type_msg = [
    "",
    "error",
    "warn",
    "msg",
    "info",
    "debug"
    ]

last_flags = []

lang_database = { '.c' : 'c' }
lang_database.update({k:'c++' for k in [".cpp", ".cc", ".cxx", ".c++", ".C"]})


class CompositeException(Exception):
    def __init__(self, message, excp_list):
        self.message = message
        self.excp_list = excp_list

    def visit_exceptions(self, fn):
        fn(self.message + ':')
        for e in self.excp_list:
            fn("  method {} failed with error: {}".format(*e))

    def __str__(self):
        message_lst = []
        self.visit_exceptions(message_lst.append)
        return "\n".join(message_lst) # might be enhanced.

class Logger:
    class ActualLogger:
        def __init__(self, f):
            self.f = f

        def __del__(self):
            self.f.write("\n")
            self.f.close()

        def __lshift__(self, obj):
            self.f.write(str(obj))
            return self

    class FakeLogger:
        def __init__(*args):
            pass

        def __lshift__(self, *args):
            return self

    def __init__(self, loglevel=LOG_WARN, filename="debug.log"):
        self.loglevel = loglevel
        self.filename = filename

    def __call__(self, loglevel):
        if loglevel <= self.loglevel:
            f = open(self.filename, "a")
            f.write("{} {:6s}: ".format(ctime(), log_type_msg[loglevel].upper()))
            return self.ActualLogger(f)
        else:
            return self.FakeLogger()


def get_ext(filename):
    return os.path.splitext(filename)[1]


def get_lang(filename):
    return lang_database.get(get_ext(filename))


def find_compiler_commands(lines):
    return filter(lambda s: re.match("^[-\\w]*g(cc|\\+\\+)", s), lines)


def find_compiler_command(lines):
    it = find_compiler_commands(lines)
    try:
        return next(it)
    except StopIteration:
        return None


def collect_flags(line):
    splitted = shlex.split(line)
    it = filter(lambda s: re.match("^-([DIW]|std=)", s), splitted)
    return list(it)


def extract_build_info_intelligent(log, filename):
    relfname = os.path.relpath(filename, os.getcwd())
    return extract_build_info_with_make(log, ['-n', "{}.build".format(relfname)])


def extract_build_info_dumb(log, filename):
    return extract_build_info_with_make(log, ['-n', '-B'])


def extract_build_info_with_make(log, cmdlist):
    raw_str = subprocess.check_output(['make'] + cmdlist, timeout=2).decode("utf-8")
    log(LOG_DEBUG) << "Output: {}".format(raw_str)

    lines = raw_str.splitlines()
    line = find_compiler_command(lines)

    log(LOG_DEBUG) << "Compiler line: " << repr(line)

    if line is None:
        raise RuntimeError("can't find compiler line in process output")

    flags = collect_flags(line)
    return flags


def try_extract_flags(log, filename):
    known_methods = [extract_build_info_intelligent,
                     extract_build_info_dumb]
    exceptions = []

    for method in known_methods:
        try:
            log(LOG_DEBUG) << "Attempting method: " << method
            flags = method(log, filename)
            return flags
        except Exception as e:
            log(LOG_MSG) << "Got error during attempt to detect lines: " << e
            exceptions.append((method, e))

    raise CompositeException("Failed to get build info", exceptions)


def FlagsForFile(filename, **kwargs):
    global last_flags
    succeeded = False

    log = Logger(LOGLEVEL, filename=".debug.log")
    log(LOG_INFO) << "Called with '{}'".format(filename)

    flags = last_flags

    lang = get_lang(filename) or "c++"

    try:
        last_flags = try_extract_flags(log, filename)
    except CompositeException as e:
        log(LOG_ERROR) << "All attempts to extract flags failed:"
        e.visit_exceptions(lambda msg: log(LOG_ERROR) << "  " << msg)
    except Exception as e:
        log(LOG_ERROR) << "Unhandled exception: " << e
    else:
        succeeded = True

    if not succeeded:
        log(LOG_WARN) << "Using last flags from previous invokation (or default ones)"

    flags = ['-x', lang] + last_flags
    log(LOG_MSG) << "flags: {}".format(flags)
        
    return {
            'flags': flags,
            'do_cache': False
            }
